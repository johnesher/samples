# Command-line tool to test the automation interface to a T computer.
#
# To run with the correct python for serial on the dev laptop start with
# "C:\program files\Python27\python" testauto2.py com1
#
# 26Mar15 - protect a if no chars typed
# 14Aug15 - fix bug in serial

import time
import socket
import re
import sys
from itertools import takewhile

class IPauto :
    def __init__( self, IPaddress=( '192.1.0.11:35000' ) ) :
        AddrPort = IPaddress.split( ':' ) + ['35000'] # force default if omitted
        self.IPaddress = ( AddrPort[0], int( AddrPort[1] ) ) # port must be integer
        self.Connect( )

    def Connect( self ) :
        ''' Connect to the T system                '''
        self.HostAddress = socket.gethostbyname(socket.gethostname())
        print 'My address is ', self.HostAddress
        print 'Trying to connect to ', self.IPaddress,
        self.Socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.Socket.settimeout( 5.0 )
        try :
            self.Socket.connect( self.IPaddress ) # Note that connect_ex returns error no instead of raising
            print ' - connected'
        except :
            print ' - connection failed'
            raise

    def write( self, dat ) :
        if not self.Socket :
            print 'Trying to reopen the socket'
            self.Connect( )
        try :
            print 'IP sending ', map(hex,map(ord,dat))
            d = ''.join( dat )
            self.Socket.send( d )
        except :
            print map( str,  sys.exc_info() )
            print 'failed - closing socket'
            self.Socket.close( )
            self.Socket = None

    def readbytes(self) :
        try :
            print 'IP receiving '
            return map( hex, map( ord, self.Socket.recv(4096) ) )
        except :
            print map( str,  sys.exc_info() )
            print 'read failedt'

    def close( self ) :
        self.Socket.close( )

class SerAuto( ) :
    def __init__( self, Port='0' ) :
        try :
            import serial
        except :
            print '**This build only works with the network interface'
            print '**Could not import serial interface module'
            #print ''''you must cd to this folder and then use
            #    C:\JTR>C:\Python25\python testauto.py'''
            raise
        port = int( re.search( '(\d+)', Port, re.I ).group(0) ) - 1
        s = serial.Serial( port )
        s.baudrate = 9600
        s.databits = 8
        s.parity = serial.PARITY_NONE
        s.timeout=1
        self.s = s
        print 'Com port %d opened' % (port + 1)

    def write( self, dat ):
        print 'Serial sending', map(hex,map(ord,dat))
        self.s.write(''.join( dat ))

    def readbytes(self) :
        if self.s.inWaiting() > 0 :
            print 'inWaiting', self.s.inWaiting()
            return map(hex,map(ord, self.s.read( self.s.inWaiting() ) ) )
        else :
            return ''

    def sendbytes( self, number ) :
        print 'sending ', number
        for i in range( number ) :
            self.s.write( chr( i & 0xff ) )

class TestChannel( ) :

    def write( self, dat ) :
        print ' dat= ', map(hex,map(ord,dat))

    def readbytes(self) :
        return 'readbytes not implemented for test yet'

def fixChecksum( cmd ) :
    cmd = cmd[:-1] + [ 0xff & ( 0x40 - ( 0xff & sum( cmd[ : -1 ]  ) ) ) ]
    return cmd

def toChr( cmd ) :
    return map( chr, cmd )

def addChecksums( cmds, checksum=True ):
    if checksum :
        tosend = map( fixChecksum, cmds )
    else :
        tosend = cmds
    return tosend

def openComms( ComsSpec ) :
    if '.' in ComsSpec :
        channel = IPauto( ComsSpec )
    elif '*' in ComsSpec :
        channel = TestChannel()
        print '\n'
        print 'TEST MODE NO COMMANDS ARE BEING SENT'
        print '\n'
    else :
        channel = SerAuto( ComsSpec )
    return channel

IpAddressPattern = re.compile( '(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(:\d+)?', re.I )
ComPortPattern = re.compile( 'com(\d+)', re.I )
TestPortPattern = re.compile( '(\*)', re.I )

def IsCommsSpec( str ) :
    return re.match( IpAddressPattern, str ) is not None \
            or re.match( ComPortPattern, str ) is not None \
            or re.match( TestPortPattern, str ) is not None

def getSender( comms ) :
    def sendit( *args, **kwargs ) :
        # If args is not a list of lists, wrap it in a list
        checksum = kwargs['checksum'] if 'checksum' in kwargs else True
        if type( args[0][0] ) is not type( [] ) :
            wrapped = list(args)
        else :
            wrapped = args[0]
        chkd = addChecksums( wrapped, checksum )
        for c in chkd :
            dat = toChr( c )
            comms.write( dat )
    return sendit

def getReader( comms ):
    return comms.readbytes

def wait( tim=3 ) :
    ''' Waits with a loop to give ctrl-C a chance '''
    if tim > 1 :
        wait( tim - 1 )
        time.sleep( 1 )
    else :
        time.sleep( tim )

def Target ( show=0, shot=0, cmd='f', cam=0 ):
    cmd = { 'f':0x01, 'fm':0x21, 'c':0x02, 'cm':0x22, 'e':0x06, 'd':0x05, 'p':0x03 }[ cmd ]
    return [ 0xf9, 1, cmd, cam, show, shot, 0, 0, 99 ]

def fade( show=1, shot=1 ) :
    return Target( show, shot, cmd='f' )

def fadem( show=1, shot=1 ) :
    return Target( show, shot, cmd='fm' )

def cut( show=1, shot=1 ) :
    return Target( show, shot, cmd='c' )

def cutm( show=1, shot=1 ) :
    return Target( show, shot, cmd='cm' )

def enable( cam ) :
    return Target( cmd='e', cam=cam )

def disable( cam ) :
    return Target( cmd='d', cam=cam )

def poll( ) :
    return Target( cmd='p' )

newpoll = Target( cmd='p' )

def badascii( ) :
    return [ 0x30, 0x31, 0x32, 0x33, 0x34 ]

def rubbish( ) :
    return [56,34,67]

def ManualRecall( txt, csum ) :
    type = txt[0] # f c
    rtxt = txt[1:]
    if txt[1] == 'm' :
        type = txt[0:2]
        rtxt = txt[2:]
    show, shot = rtxt.split(',')
    send( Target( int(show), int(shot), type ), checksum=csum )


def handleUser( c, csum  ) :
    if 'p' in c[0] :
        send( poll(), checksum=csum )
        print 'read poll reply', reader( )
    if 'a' in c[0] and len(c[1:]) > 0 :
        send( [ord(x) for x in c[1:] ], checksum=False )
    if 'b' in c[0] :
        send( rubbish(), checksum=False )
    if 'e' in c[0] :
        send( enable( int(c[1]) ), checksum=csum )
    if 'd' in c[0] :
        send( disable( int(c[1]) ), checksum=csum )
    if c[0] in 'fc' :
        ManualRecall( c, csum )
    if 'x' in c[0] :
        execfile( c[1:] )
    if 'k' in c[0] :
        return not csum
    return csum

def UserInput( ) :
    prompt = '\n\t'.join( [
        '\n\ta send typed ascii',
        'b bad block',
        'cx,y cut to show x, shot y',
        'cmx,y cut to show x, shot y with move',
        'fx,y fade to show x, shot y',
        'fmx,y fade to show x, shot y with move',
        'p - poll',
        'eC - enable C',
        'dC - disable C',
        'k - toggle checksum from %d',
        'xfilename - to execute the file',
        'q to quit '])
    csum = True
    print 'MANUAL MODE'
    c = 'continue'
    while c[0] != 'q' :
        c = raw_input( prompt % csum)
        csum = handleUser( c, csum )
    print 'Quit'


HelpText = """
    You can give command line arguments to control the application.
    The first one is to select ip address or com port.
    An item of the form ddd.ddd.ddd.ddd:pppp will select network mode
    An item of the form comD will select serial on port D
    An item with just a * will select test mode where no comms is used.

    Any following items are taken to be the name of a file of commands to execute.
    The commands are in Python.

    To run with the correct python for serial on the dev laptop start with
    "C:\program files\Python27\python" testauto2.py com1
    
    If there is no file the app runs in manual mode and accepts the following
    commands
        'a - send the following characters as ascii',
        'b - send a bad block (currently three chars)',
        'cx,y - cut to show x, shot y',
        'cmx,y - cut to show x, shot y with move',
        'fx,y - fade to show x, shot y',
        'fmx,y - fade to show x, shot y with move',
        'p - send a poll',
        'eC - enable camera C',
        'dC - disable camera C',
        'k - toggle checksum on and off (starts on)',
        'xfilename - to execute the file',
        'q to quit'
    """

if __name__ == '__main__' :
    if len( sys.argv) > 1 and 'help' in sys.argv[1] :
        print HelpText
        quit()
    commsSpec = [ x for x in sys.argv[1:] if IsCommsSpec( x ) ] + ['*'] # force default
    files = [ x for x in sys.argv[1:] if not IsCommsSpec( x ) ]
    comms = openComms( commsSpec[0].rstrip(',') )
    send = getSender( comms )
    reader = getReader( comms )
    if len( files ) :
        map( execfile, files )
    else :
        UserInput( )
    print 'closed'
            
