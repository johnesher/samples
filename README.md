# Samples #

This is a few samples of code that I have written which I am able to put onto a public web site. Most of my code contains proprietary information and as such cannot be published.

## TaitStegWizard ##
This is a python implementation of a wxWindows wizard class. There is a C implementation of a wizard class but I found it too restrictive for my purposes so created this Python implementation instead. Unfortunately time prevented me from generalising it properly so it is somewhat focussed on my needs at that time but could be used as a basis for a generic wizard.

## TestAuto ##
This is a test tool using the python serial interface to control an application. It is written with a functional approach.