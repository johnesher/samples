import wx
#import  Bitmaps
import logging
import threading
import pprint
import sys
from RoboBossFrame import StegWizPagesPanel, WizDialog

pp = pprint.pprint

log = logging.getLogger( 'Cmc' )

class EndableDialog(wx.Dialog) :

    def __init__(self, parent) :
        wx.Dialog.__init__(self, None, -1, 'Waiting for position response message from head' )

    def die(self) :
        self.EndModal(wx.ID_CANCEL)

class TimeoutDialog(wx.Dialog) :
    """ Shows a message for a period and then closes
        Optionally shows a progress bar as well
        """

    def __init__(self, parent) :
        wx.Dialog.__init__(self, parent, -1, 'Waiting for position response message from head' )
        self.MainSizer = wx.BoxSizer( wx.VERTICAL )
        #self.Gauge = wx.Gauge( self, wx.ID_ANY, 20, wx.DefaultPosition, (400,20))
        #print 'parentxy', parent.GetScreenPosition(), parent.GetPosition()
        self.Gauge = wx.Gauge( self, wx.ID_ANY, 20, parent.GetScreenPosition(), (400,20))
        self.MainSizer.Add( self.Gauge, 1, wx.EXPAND |wx.ALL, 5 )
        self.SetSizer( self.MainSizer )
        self.Layout()
        self.MainSizer.Fit( self )
        self.Centre( wx.BOTH )
        self.count = 0
        self.Bind(wx.EVT_TIMER, self.TimerHandler)
        self.timer = wx.Timer(self)
        self.timer.Start(100)

    def __del__(self):
        self.timer.Stop()

    def TimerHandler(self, event):
        try :
            self.count = self.count + 1
        except :
            log.error(' StegWiz errorintimerhandler' )
            self.die()
        self.Gauge.Pulse()
        if self.count >= 20:
            self.timer.Stop()
            self.die()

    def die(self) :
        self.timer.Stop()
        if self.IsModal() : 
            self.EndModal(wx.ID_CANCEL) 
        else : 
            self.Show(False)


class Wizard( WizDialog ) :
    ''' Base class for all wizards
        '''

    def __init__(self, *args,**kwargs) :
        super(Wizard,self).__init__(*args, **kwargs)
        self._currentPage=None
        self._cancelEnabled = True

    def RunWizard(self, firstPage ) :
        self.wizSizerClient.Add(firstPage)
        self._currentPage=self._changePage(firstPage)
        return self.ShowModal()

    def _changePage(self, page ) :
        if self._currentPage :   # guard on initialisation
            self._currentPage.Hide()
            self._currentPage.Disable() # managed to get old page taking events
        self.wizSizerClient.Detach( 0 )#oldPageItem)
        self.wizSizerClient.Add( page, 1 )
        self.wizNext.Label='&Next >' if page.GetNext() else '&Finish'
        page.Show() # MUST show before layout
        page.Enable()
        self.Layout() # dialog
        return page

    def SetImage( self, image ) :
        h,w=self.wizBitmap.GetSize()
        image.Rescale(h,int(h*image.GetHeight()/float(image.GetWidth())))
        self.wizBitmap.SetBitmap( wx.BitmapFromImage(image) )

    def OnWizBackClicked( self, event ):
        if self._currentPage.GetPrev() :
            if self.OnWizPageChanging(0,self._currentPage) :
                self._currentPage = self._changePage( self._currentPage.GetPrev() )
                self.EnableNext(True)
                self.OnWizPageChanged(0,self._currentPage)
        else :
            log.debug( 'StegWiz OnWizBackClicked but next page is none' )

    def OnWizNextClicked( self, event ):
        if self._currentPage.GetNext() :
            if self.OnWizPageChanging(1,self._currentPage) :
                self._currentPage = self._changePage( self._currentPage.GetNext() )
                self.EnableBack(True)
                self.OnWizPageChanged(0,self._currentPage)
        else :
            self.EndModal(wx.ID_OK)

    def OnWizCancelClicked( self, event ):
        self._currentPage.cancelClicked()
        self.EndModal(wx.ID_CANCEL)

    def OnWizHelpClicked( self, event ):
        msg =self._currentPage.WizHelp
        msg += 'No help available' if len(msg) == 0 else ''
        wx.MessageBox( msg, 'Help is being provided' )

    def OnWizCloseDialog( self, event ):
        self._currentPage.cancelClicked()
        self.EndModal(wx.ID_CANCEL)

    def EnableBack( self, state=True ) :
        self.wizBack.Enable(state)

    def EnableNext( self, state=True ) :
        self.wizNext.Enable(state)

    def EnableChange(self, state=True) :
        self.EnableNext(state)
        self.EnableBack(state)

    def EnableCancel(self, state=True) :
        oldState=self._cancelEnabled
        self._cancelEnabled=state
        return oldState # allow caller to store and reset state


#----------------------------------------------------------------------

MINIMUMBITMAPWIDTH=100

class TitledPage( wx.Panel ):
    def __init__(self, parent, title, subTitle, panel, pageName=None):
        super(TitledPage,self).__init__( parent)
        self.parent=parent
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)
        self.title = wx.StaticText(self, -1, title,wx.DefaultPosition, wx.DefaultSize, 0)#wx.SUNKEN_BORDER)
        self.title.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        sizer.Add(self.title, 0, wx.EXPAND|wx.ALIGN_CENTRE_HORIZONTAL|wx.ALL, 5)

        self.subTitle = wx.StaticText(self, -1, subTitle,wx.DefaultPosition, wx.DefaultSize, 0)
        self.subTitle.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))
        sizer.Add(self.subTitle, 0, wx.EXPAND|wx.ALIGN_CENTRE_HORIZONTAL|wx.ALL, 5)
        sizer.Add(wx.StaticLine(self, -1), 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        self.pageTitle=title
        self.pageName=pageName
        self.SetPanel( panel )
        self._next = self._prev = None
        self.WizHelp='' # null string means no help

    def SetPanel( self, panel ) :
        panel.Reparent(self)
        if panel :
            self.Sizer.Add(panel, 1, wx.EXPAND|wx.ALL, 5)
        self.panel=panel

    def setSubTitle( self, subTitle ) :
        self.subTitle.SetLabel( subTitle )

    def setTitle( self, title ) :
        self.title.SetLabel( title )

    def SetNext(self, _next):
        self._next = _next

    def SetPrev(self, _prev):
        self._prev = _prev

    def GetNext(self):
        return self._next

    def GetPrev(self):
        return self._prev

    def HandleLeaving( self, dx, page ) :
        return True

    def HandleEntering( self, dx, page ) :
        pass

    def cancelClicked(self) :
        pass

#----------------------------------------------------------------------


class ChooseCamera( TitledPage ) :
    def __init__(self, parent, title, panel, pageName=None):
        super(ChooseCamera, self).__init__( parent, 'Choose Camera', title, panel, pageName )
        self.parent.EnableBack( False ) # HandleEntering not called when first showing the page

    def HandleEntering( self, dx, page ) :
        self.setSubTitle('')
        self.parent.EnableBack( False )

    def HandleLeaving( self, dx, page ) :
#        txt=page.FindWindowByName('radioBoxCamera').GetStringSelection()
        txt=self._radioBoxCamera.GetStringSelection()
        a,b=txt.split()
        self.parent.headNo = int( b )
        self.parent.pageTitle='Head %d' % self.parent.headNo
        return True

    def OnRadioBoxCamera(self, evt ) :
        self.parent.EnableNext()

    def createCameraChoicesRadioBox( self, onlineCams, selectedCams) :
        radioBoxCameraChoices = [ 'Camera %d' % c for c in sorted(onlineCams)+[0] ]
        oldWin=self.FindWindowByName('m_radioBoxCamera')
        location = oldWin.ContainingSizer.ContainingWindow
        rows = min( 4,int(len(onlineCams)**0.5))
        radioBoxCamera = wx.RadioBox(location , wx.ID_ANY, u"Choose Camera", wx.DefaultPosition, wx.DefaultSize,\
                    radioBoxCameraChoices, rows, wx.RA_SPECIFY_ROWS, wx.DefaultValidator, u"radioBoxCamera" )
        oldWin.ContainingSizer.Replace(oldWin, radioBoxCamera)
        if len(selectedCams) : 
            radioBoxCamera.SetSelection( sorted(onlineCams).index(sorted(selectedCams)[0]) )
        else : # hide selection
            radioBoxCamera.SetSelection( radioBoxCamera.GetCount()-1)
            self.parent.EnableNext( False )
        radioBoxCamera.ShowItem( radioBoxCamera.GetCount()-1, False ) # hide the hidden entry
        radioBoxCamera.Bind( wx.EVT_RADIOBOX, self.OnRadioBoxCamera )
        self._radioBoxCamera=radioBoxCamera
        oldWin.Destroy()


class ChooseAxis( TitledPage ) :
    def __init__(self, parent, title, panel, pageName=None):
        super(ChooseAxis, self).__init__( parent, 'Choose Axis', title, panel, pageName )
        self.radioBox = panel.FindWindowByName('m_radioBoxAxis')
        self.radioBox.Bind( wx.EVT_RADIOBOX, self.OnRadioBoxChooseAxis )
        self.radioBox.EnableItem(5,False)
        self.radioBox.EnableItem(6,False)
        self.axisCodes=['h12', 'p1819', 'p27', 't1819', 't27','std', 'sps']

    def GetNext(self) :
        ctrl=self.radioBox
        self.parent.axisText =ctrl.GetStringSelection().replace('&','')
        self.parent.axisCode=self.axisCodes[ ctrl.GetSelection() ]
        # append axis but remove any previous axis incase we are reversing
        self.parent.pageTitle = self.parent.pageTitle.split(',')[0] +', %s' % self.parent.axisText
        if 'h' in self.parent.axisCode :
            pages = self.parent.heightPages
        elif 'p' in self.parent.axisCode:
            pages = self.parent.panPages
        else :
            pages = self.parent.tiltPages
        self.parent.setupPageOrder(pages) 
        return self._next

    def OnRadioBoxChooseAxis(self, evt ) :
        self._setImage()

    def _setImage(self) :
        ctrl=self.radioBox
        axisCode=self.axisCodes[ ctrl.GetSelection() ]
        self.parent.setImageFromAxisCode(axisCode)

    def HandleEntering( self, dx, page ) :
        self._setImage()


class ChooseHeightLocation( TitledPage ) :
    def __init__(self, parent, title, panel, pageName=None):
        super(ChooseHeightLocation, self).__init__( parent, 'Where is the height?', title, panel, pageName )
        self.radioBoxLocation = panel.FindWindowByName('m_radioBoxLocation')
        self.radioBoxLocation.Bind( wx.EVT_RADIOBOX, self.OnRadioBoxLocation )
        self.textCtrlHeightCounts = panel.FindWindowByName('enterCounts')
        self.textCtrlHeightCounts.Bind( wx.EVT_KEY_UP, self.OnHeightCountsKeyUp )
        self.heightTop = (6000,1032) # counts, mm
        self.heightBtm = (197350,10)
        self.valid = True

    def HandleEntering( self, dx, page ) :
        self.setHeightEntry( page )

    def setHeightEntry(self, page ) :
        choice = self.radioBoxLocation.GetSelection()
        entry = self.textCtrlHeightCounts
        #setting = page.FindWindowByName('m_textSetting')
        self.heightInCounts = choice != 3
        entry.Enable( choice>=2 )
        if choice <2 :
            entry.SetValue( str( [ self.heightTop[0], self.heightBtm[0] ] [choice ] ) )
        if len(entry.GetValue()) > 0 :
            self.valid = self._setHeightCounts( int(entry.GetValue()), self.heightInCounts )

    def HandleLeaving( self, dx, page ) :
        if dx : # forward
            if not self.valid : 
                return False
            choice = self.radioBoxLocation.GetSelection()
            if choice == 3 : # height in mm
                entry = self.textCtrlHeightCounts
                self.parent.location = '%dmm' % int(float('0'+entry.Value))
            else :
                self.parent.location = ['Top', 'Bottom', 'in Counts', 'in mm' ][ choice ]
            self.parent.newStegSetting = int(page.FindWindowByName('m_textSetting').GetLabel() )
        return True

    def OnRadioBoxLocation(self, evt) :
        page = evt.GetEventObject().GetParent()
        self.setHeightEntry( page )

    def OnHeightCountsKeyUp( self, evt ) :
        txt=evt.GetEventObject().Value
        try:
            val = float('0'+txt)
        except: # remove the offensive character
            evt.GetEventObject().SetValue( ''.join([ c for c in txt if c in '0123456789' ]) )
            evt.GetEventObject().SetInsertionPointEnd()
            return
        self.valid = self._setHeightCounts( int('0'+evt.GetEventObject().Value), self.heightInCounts )

    def _setHeightCounts( self, val, inCounts ) :
        if inCounts :
            new = val
            valid = val <= 204800
        else :
            rangemm = float(self.heightTop[1] - self.heightBtm[1])
            rangecounts = self.heightBtm[0] - self.heightTop[0]
            new = int( (self.heightTop[1]-val)*rangecounts/rangemm + self.heightTop[0] )
            valid = val <= self.heightTop[1] and val >= self.heightBtm[1]
        self.FindWindowByName('m_textSetting').SetLabel(' '+str(new)+' ')
        self.textCtrlHeightCounts.SetForegroundColour( [(255,0,0,255), wx.NullColour][ valid ] )
        self.Refresh()
        return valid


class ConfirmSetting( TitledPage ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(ConfirmSetting, self).__init__( parent, 'Confirm the Setting', title, panel, pageName )

    def HandleEntering( self, dx, page ) :
        txtStr='%s is located at %s\n\nand has an encoder reading of %d.\n\nThe encoder will be reset to %d.\n\nClick on Next to reset the encoder.'
        txt1 = page.FindWindowByName('m_staticTextTarget')
        txt2 = page.FindWindowByName('m_textWarnRange')
        posn = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if posn is not None :
            txt1.SetLabel( txtStr % (self.parent.axisText, self.parent.location, posn, self.parent.newStegSetting) )
            txt2.Show(abs( posn-self.parent.newStegSetting) > 10000 )
            self.parent.EnableBack(True)
        else :
            txt1.SetLabel( 'Unable to get current position from the head' )
            txt2.Show(False)
            self.parent.EnableBack(False)
        # for some unknown reason the back button became focussed on only this page 
        # and the only way I could find to put the focus on the next button is to do it manually
        if dx :
            self.parent.wizNext.SetFocus()

    def HandleLeaving( self, dx, page ) :
        ''' Change the back of the next page to our previous page
            so that this page is skipped when moving backwards.
            '''
        if dx : # is forward
            param = { 'ifcPan':0, 'ifcTilt':0, 'ifcHeight':0 }
            axis = [ ['ifcHeight', 'ifcTilt']['t' in self.parent.axisCode], 'ifcPan' ]['p' in self.parent.axisCode]
            param[axis]=int(self.parent.newStegSetting)
            self.parent.setStegmanns( [self.parent.headNo], param )
        beforeMe=self.GetPrev()
        self.GetNext().SetPrev(beforeMe)
        return True


class ProgressSetting( TitledPage ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(ProgressSetting, self).__init__( parent, 'Updating', title, panel, pageName )
        self.gauge = panel.FindWindowByName('m_gaugeResetting')
        self.Killed = threading.Event( )
        self.m_staticTextEnd=panel.FindWindowByName('m_staticTextEnd')
        self.m_staticTextTarget=panel.FindWindowByName('m_staticTextTarget')
        self.myTimer=None
        self.myTimerCounter=0

    def HandleEntering( self, dx, page ) :
        self.gauge.SetValue(0)
        self.parent.EnableBack(False)
        self.parent.EnableNext(False)
        #txt = page.FindWindowByName('m_staticTextTarget')
        txt = self.m_staticTextTarget
        posn = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if posn is None :
            txt.SetLabel( 'Unable to get current position from the head' )
            self.parent.EnableBack(True)
        else :
            txt.SetLabel( '%s is located at %s\n\nand has an encoder reading of %d.'\
                        '\n\nThe encoder is being reset to %d.'\
                        % (self.parent.axisText, self.parent.location, posn, self.parent.newStegSetting) )
            self.m_staticTextEnd.Show(False)
            self.gauge.Show(True)
            self.parent.Layout()
            #self.Killed = threading.Event( )
            self.Killed.clear() # ensure clear from any previous entry to this page
            self.HeadBack=False
            self.myTimerCounter=0
            self.StartPosn=posn
            self.RestartTimer()

    def RestartTimer(self):
        self.myTimer = threading.Timer( 1, self.timer )
        self.myTimer.setName( 'Wizard Progress Timer' )
        self.myTimer.start()

    def timer( self ) :
        """ Show progress to the user by moving the progress indicator. Wait for the head
            to restart and then stop the timer from restarting itself.
            """
        print 'self.myTimerCounter', self.myTimerCounter
        # TODO: timeout does not cleanup
        if self.Killed.isSet( ) : #  User cancelled so don't restart the timer
            return
        self.myTimerCounter += 1
        try :
            self.gauge.SetValue((self.gauge.GetValue()+5) % 100)
            # Only calls getonlineCams if counter > limit to give the head time to go offline
            if self.myTimerCounter > 6 and self.parent.headNo in self.parent.GetOnlineCams() :
                wx.CallAfter(self.handleHeadBack) # call on main thread not on timer thread
            elif self.myTimerCounter > 30:
                wx.CallAfter(self.handleTimeout) # call on main thread not on timer thread
            else :
                self.RestartTimer()
        except :
            xtype, errmess, xtraceback = sys.exc_info( )
            raise
            # TODO: log.error( 'Exception in Wizard timer', exc_info=True )

    def cancelClicked(self) :
        self.Killed.set( ) 
        #self._closedown()
        if not self.HeadBack :
            wx.MessageBox( 'The reset procedure had started so the setting of the encoder '\
                                    'has probably changed', 'Cancelled before head back online',
                                    style=wx.ICON_EXCLAMATION ) 

    def _closedown(self):
        if self.myTimer : self.myTimer.cancel()
        self.myTimer=None

    def handleHeadBack( self ) :
        ''' After the head has had time to reset, get the new value and display it to the user.
            '''
        # This will try to select the camera before getting the position
        self.HeadBack=True
        posn = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if posn is None :
            self.m_staticTextEnd.SetLabel( 'Unable to get position from the head' )
        else :
            txtStr='%s was at %d.\n\nIt is now set to %d.\n\n'\
                        'If this is correct click Finish or click Back to repeat the procedure.' 
            self.m_staticTextEnd.SetLabel( txtStr % (self.parent.axisText, self.StartPosn, posn) )
        self._tidyup()

    def _tidyup(self) : 
        self.m_staticTextEnd.Show(True)
        self.restoreButtons()
        self.m_staticTextTarget.Show(False)
        self.gauge.Show(False)
        self.parent.Layout()

    def handleTimeout(self) :
        ''' Got bored waiting for head to come back
            '''
        self.m_staticTextEnd.SetLabel( 'TIMEOUT waiting for head to restart.' )
        self._tidyup()

    def HandleLeaving( self, dx, page ) :
        self.restoreButtons()
        self.Killed.set( ) 
        #self._closedown()
        return True

    def restoreButtons(self) :
        self.parent.EnableBack(True)
        self.parent.EnableNext(True)


class PanTiltLocationBase( TitledPage ) :
    def __init__(self, parent, title, subTitle, panel, pageName=None):
        super(PanTiltLocationBase, self).__init__( parent, title, subTitle, panel, pageName )
        self.radioBoxLocation = panel.FindWindowByName('m_radioBoxLocation')
        self.radioBoxLocation.Bind( wx.EVT_RADIOBOX, self.OnPanTiltRadioBox )
        self.textCtrlHeightCounts = panel.FindWindowByName('enterCounts')
        self.textCtrlHeightCounts.Bind( wx.EVT_KEY_UP, self.OnPanTiltKeyUp )
        self.staticTextSetting = panel.FindWindowByName('m_textSetting')
        self._presets = [4, 5, 6]
        self.headPosn = None # set in HandleEntering

    def _getStringChosen( self, page=None ) :
        choice = self.radioBoxLocation.GetSelection()
        return self.radioBoxLocation.GetString( choice ).replace('&','') # settingMap[ choice ]

    def _getCurrentSetting( self ) :
        txt = self.staticTextSetting.GetLabel().strip()
        try :
            val=int(txt)
        except :
            log.debug( 'StegWiz Bad setting %r' % txt )
            val = 0
        return val
        #return int(txt) if len(txt) else 0

    currentSetting = property( fget=_getCurrentSetting )

    def Valid( self ) :
        choice = self.FindWindowByName('m_radioBoxLocation').GetSelection()
        # choice==3 is for Factory so setting is not relevant
        return (choice == 3) or ( 0 < self.currentSetting <= 204800 )

    def _leavingPanTiltLocation( self, dx,page,settingMap) :
        if not self.Valid() :
            return False # veto move
        if dx : # forward
            self.parent.location = self._getStringChosen( ) # settingMap[ choice ]
            self.parent.newStegSetting = self.currentSetting
        return True

    def OnPanTiltKeyUp( self, evt ) :
        txt=evt.GetEventObject().Value
        if len(txt) == 0 : return
        choice = self.FindWindowByName('m_radioBoxLocation').GetSelection()
        if choice==5 : # Offset so allow signed
            posn = self.headPosn
            allowedChars='-0123456789'
        else :
            posn=0
            allowedChars='0123456789'
        try:
            val = float(txt)
        except: # remove the offensive character (might be trailing -)
            txt2 = txt[0]+txt[1:].replace('-','') if len(txt) > 1 else txt
            evt.GetEventObject().SetValue( ''.join([ c for c in txt2 if c in allowedChars ]) )
            evt.GetEventObject().SetInsertionPointEnd()
            #return
        else : # txt is a legal float
            page = evt.GetEventObject().GetParent()
            page.FindWindowByName('m_textSetting').SetLabel( ' '+str(int(val+posn))+' ')
            evt.GetEventObject().SetForegroundColour( [ (255,0,0,255) ,wx.NullColour][self.Valid()] )
            self.parent.EnableChange(self.Valid())
            self.Refresh()

    def GetNext(self,settingMap) :
        location = self._getStringChosen( self.panel )
        bothPages=[self.parent.panFactoryPages, self.parent.tiltFactoryPages]\
                        if 'factory' in location.lower() else [self.parent.panPages, self.parent.tiltPages]
        pages = bothPages[ 't' in self.parent.axisCode ]
        self.parent.setupPageOrder(pages) 
        return self._next

    def OnPanTiltRadioBox(self, evt ) :
        page = evt.GetEventObject().GetParent()
        self._setAppropriateLabel( page )

    def HandleEntering( self, dx, page ) :
        self.headPosn = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if self.headPosn is None : self.headPosn=0
        self._setAppropriateLabel( self )

    def _setAppropriateLabel( self, page ) :
        choice = page.FindWindowByName('m_radioBoxLocation').GetSelection()
        entry = page.FindWindowByName('enterCounts')
        setting = page.FindWindowByName('m_textSetting')
        if choice == 3 : # factory
            setting.SetLabel( ' ' )
            entry.Disable()
        elif choice==4 : #Entry
            setting.SetLabel( (' '+entry.Value+' ').replace('-','') )
            entry.Enable()
        elif choice==5 : #Offset
            setting.SetLabel( ' '+entry.Value+' ' )
            entry.Enable()
        else :
            setting.SetLabel( ' ' + str( self._presets[choice] ) + ' ' )
            entry.Disable()
        self.parent.EnableChange(self.Valid())


class ChoosePanLocation( PanTiltLocationBase ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(ChoosePanLocation, self).__init__( parent, 'Where is the Pan?', title, panel, pageName )
        self.settingMap=['Clockwise', 'Straight forwards', 'Anticlockwise', 'Factory', 'Position', 'Offset' ]
        self._presets = [4000, 102400, 204800-4000]
        self.WizHelp='''Explain about factory and offset'''

    def HandleLeaving( self, dx, page ) :
        return self._leavingPanTiltLocation( dx,page,self.settingMap)

    def GetNext( self ) :
        return super(ChoosePanLocation, self).GetNext(self.settingMap)


class ChooseTiltLocation( PanTiltLocationBase ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(ChooseTiltLocation, self).__init__( parent, 'Where is the Tilt?', title, panel, pageName )
        self.settingMap=['Up', 'Horizontal', 'Down', 'Factory', 'Position', 'Offset' ]

    def HandleEntering( self, dx, page ) :
        if '27' in self.parent.axisCode :
            self._presets = [4000, 102400, 204800-4000]
        else:
            self._presets = [204800-77400, 102400, 77400] # TG18/TG19
        super(ChooseTiltLocation, self).HandleEntering(dx,page)

    def HandleLeaving( self, dx, page ) :
        return self._leavingPanTiltLocation( dx,page,self.settingMap)

    def GetNext( self ) :
        return super(ChooseTiltLocation, self).GetNext(self.settingMap)


class FactoryProcedureBase( TitledPage ) :
    def __init__(self, parent, title, subTitle, panel, pageName=None):
        super(FactoryProcedureBase, self).__init__( parent, title, subTitle, panel, pageName )

    def getRange( self ) :
        return (120000,70000) if 't18' in self.parent.axisCode else (204800-10000, 10000)

    def rangeCheck( self, posn ):
        if posn is None : return False
        maxP, minP = self.getRange()
        return minP < posn < maxP

    def warnUser( self, posn, msg ) :
        return wx.YES == wx.MessageBox( msg, 'Warning - Out of Normal Range',
                    style=wx.YES_NO | wx.ICON_EXCLAMATION | wx.NO_DEFAULT ) 


class FactoryProcedureFirst( FactoryProcedureBase ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(FactoryProcedureFirst, self).__init__( parent, 'First Step', title, panel, pageName )

    def HandleLeaving( self, dx, page ) :
        if not dx : return True # always allow backwards
        self.parent._factoryFirst = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if self.parent._factoryFirst is None :
            #self.warnUser( self.parent._factoryFirst,'Unable to get position from head' )
            return False
        else :
            if self.rangeCheck( self.parent._factoryFirst ) :
                return True
            else :
                a,b=self.getRange()
                msg = 'The position of %d is outside the expected limits\n'\
                        'for the axis of less than %d or greater than %d.\n'\
                        '\nContinue?' % (self.parent._factoryFirst, a,b )
                return self.warnUser( self.parent._factoryFirst, msg )


class FactoryProcedureSecond( FactoryProcedureBase ) :

    def __init__(self, parent, title, panel, pageName=None):
        super(FactoryProcedureSecond, self).__init__( parent, 'Second Step', title, panel, pageName )


    def HandleLeaving( self, dx, page ) :
        if not dx : return True # always allow backwards
        posn = self.parent.getAxisPosition(self.parent.headNo, self.parent.axisCode)
        if posn is None :
            #self.warnUser( self.parent._factoryFirst,'Unable to get position from head' )
            return False
        else :
            self.parent.newStegSetting = ( 204800-(self.parent._factoryFirst-posn) ) / 2
            minDiff = 40000 if 't18' in self.parent.axisCode else (204800-20000)
            if self.rangeCheck( posn ) and abs( self.parent._factoryFirst-posn ) > minDiff :
                return True
            else :
                a,b=self.getRange()
                msg = 'Either the position of %d is outside the expected limits\n'\
                        'for the axis of less than %d or greater than %d\n'\
                        'or it is closer than %d to the first position.\n'\
                        '\nContinue?' % (self.parent._factoryFirst, a,b, minDiff)
                return self.warnUser( self.parent._factoryFirst, msg )


#----------------------------------------------------------------------
# Decorators no longer used.

def RegisterHandlers(cls) :
    cls._handlersOnEntering={} # dict of functions to handle page events
    cls._handlersOnLeaving={} # dict of functions to handle page events
    #hn=[c for c in dir(cls) if c.startswith('h') ]
    for methodname in dir(cls):
        method=getattr(cls,methodname)
        if hasattr(method,'IamLeavingPage'):
            cls._handlersOnLeaving[ getattr(method, 'IamLeavingPage' ) ]=getattr(cls,methodname)
        if hasattr(method,'IamEnteringPage'):
            cls._handlersOnEntering[ getattr(method, 'IamEnteringPage' ) ]=getattr(cls,methodname)
    return cls

def registerLeaving( page ) :
    def registerFunction(func) :
        func.IamLeavingPage=page
        return func
    return registerFunction

def registerEntering( page ) :
    def registerFunction(func) :
        func.IamEnteringPage=page
        return func
    return registerFunction


class StegWizard( Wizard ) : 

    def __init__(self, parent, GetOnlineCams, selectedCams, getPositions, setStegmanns) :
        super(StegWizard,self).__init__( parent)
        self.parent=parent
        self.Label = 'Tait Encoder Reset Wizard'
        self.getPositions=getPositions
        self.setStegmanns=setStegmanns
        self.GetOnlineCams=GetOnlineCams
        self.SetImage(self.getImage( 't27' ) ) # TODO: get a better start image
        pageClasses=[ChooseCamera, ChooseAxis, ChooseHeightLocation,
                            ChoosePanLocation, ChooseTiltLocation,
                            FactoryProcedureFirst, FactoryProcedureSecond,
                            ConfirmSetting, ProgressSetting ]
        pagesPanel= StegWizPagesPanel( self)#parent )
        pagesPanel.Hide()
        for pageClass in pageClasses :
            panelName=pageClass.__name__[0].lower() + pageClass.__name__[1:]
            pagePanel=pagesPanel.FindWindowByName( panelName )
            assert pagePanel is not None, 'Cannot find %s' % pagePanel 
            pageObj = pageClass(self, '', pagePanel, panelName)
            pageObj.Hide()
            setattr( self, panelName, pageObj )
        pagesPanel.Destroy()
        del pagesPanel
        pagesPanel=None

        pre = ['chooseCamera', 'chooseAxis' ]
        post = [ 'confirmSetting', 'progressSetting' ]
        factoryPages=['factoryProcedureFirst', 'factoryProcedureSecond' ]
        self.heightPages = pre + ['chooseHeightLocation'] + post
        self.panPages = pre + ['choosePanLocation'] + post
        self.panFactoryPages = pre + ['choosePanLocation'] + factoryPages + post
        self.tiltPages = pre + ['chooseTiltLocation'] + post
        self.tiltFactoryPages = pre + ['chooseTiltLocation'] + factoryPages + post
        self.setupPageOrder( self.heightPages) 
        self.chooseCamera.createCameraChoicesRadioBox( set(GetOnlineCams()), set(selectedCams()) )

        self.heightInCounts=True
        self.pageTitle='Choose Camera'
        self.headNo=None
        self.disableCancel=False
        self.location='NOTSET'
        self.myTimer=None
        self.TimerIsRunning = False
        self.waitDialog=None


    def RunWizard( self ) :
        return super(StegWizard,self).RunWizard( self.chooseCamera )

    def setupPageOrder( self, order ) :
        pages = [ getattr( self, p ) for p in order ]
        pagePairs=zip( [None]+pages, pages+[None] ) # tuples of linked pages with offset
        for t in pagePairs :
            if t[0] is not None : t[0].SetNext( t[1] ) 
            if t[1] is not None : t[1].SetPrev( t[0] )

    def setImageFromAxisCode( self, axisCode ) :
        image = self.getImage( axisCode )
        self.SetImage(image)
#        self.SetImage(wx.BitmapFromImage(image))
        self.Layout()

    def getImage(self, name ) :
        ''' Return the image matching the given name.
            Probably cache images as well
            '''
        imageName={'h12':'TI12.jpg', 'p1819':'TG18.jpg', 'p27':'TG27.jpg',
                            't1819':'TG18.jpg', 't27':'TG27.jpg'}[ name ]
        image=wx.Image( '.\\images\\%s'%imageName, wx.BITMAP_TYPE_ANY )
#        image.Rescale(MINIMUMBITMAPWIDTH,int(MINIMUMBITMAPWIDTH*image.GetHeight()/float(image.GetWidth())))
        return image

    def getAxisPosition( self, camNo, axisCode ) :
        ''' Called as a function and must not return until the position is available.
            Hangs in modal dialog until the datum is available
            '''
        if self.TimerIsRunning : # suspect recursive call
            log.debug( 'StegWiz recursivecall' )
            return None
        self.posns=None
        ignoredPositions = self.getPositions([camNo], callback=self.PositionsReceived)
        # Launch a timer to protect
        # Show a dialog as a way of pausing the application for a while and letting
        # the user know what is happening
        self.waitDialog=TimeoutDialog( self.parent )
        self.waitDialog.ShowModal()
        log.debug( 'StegWiz continuing' )
        self.waitDialog.Destroy()
        self.waitDialog=None
        if self.myTimer : self.myTimer.cancel()
        if self.posns is None : # Failed
            wx.MessageBox( 'Unable to read current position from head %d' % camNo,
                    'Unable to set Encoder',
                    style=wx.OK | wx.ICON_ERROR ) 
            # log it
            # causes exception exit from python self.EndModal(wx.ID_CANCEL) # Does return
            log.debug( 'StegWiz Forcecancelreturned' )
            return None
        else :
            selector = [ ['height', 'tilt']['t' in axisCode], 'pan' ]['p' in axisCode]
            return self.posns[selector]

    def StartTimeOut(self) :
        self.TimerIsRunning=True
        self.waitCount = 0
        self.myTimer = threading.Timer( 3, self.timer )
        self.myTimer.setName( 'Wizard Position Timer' )
        self.myTimer.start()

    def timer( self ) :
        """ Wait for the position response to have arrived.
            """
        self.TimerIsRunning=False
        if self.waitDialog : 
            self.waitDialog.die()
        return

    def OnWizPageChanged(self, dx, page):
        page.setSubTitle( self. pageTitle )
        page.HandleEntering( dx, page )

    def OnWizPageChanging( self, dx, page ) :
        self._cancelEnabled=True
        return page.HandleLeaving( dx, page )

    def PositionsReceived(self, posns) :
        self.posns=posns
        if self.waitDialog : 
            self.waitDialog.die()

    def OnRunSimpleWizard(self, evt):
        pass



class TestPanel(wx.Panel) :

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        b = wx.Button(self, -1, "Run Simple Wizard", pos=(50, 50))
        b = wx.Button(self, -1, "Run Dynamic Wizard", pos=(50, 100))
        self.Bind(wx.EVT_BUTTON, self.OnRunDynamicWizard, b)

    def OnRunDynamicWizard(self,evt):
        self.RunDynamicWizard()

    def RunDynamicWizard( self,onlineCams, selectedCams, getPositions, setStegmanns ) :
        wizard = StegWizard( self, onlineCams, selectedCams, getPositions, setStegmanns )
        ret=wizard.RunWizard()
        log.debug( 'StegWiz returned ', ['Finished','Cancelled'][ret==wx.ID_CANCEL] )
        wizard.Destroy()



#----------------------------------------------------------------------

def runTest(frame, nb, onlineCams, selectedCams, getPositions, setStegmanns):
    win = TestPanel(nb)
    print 'panel created - about to run the wizard'
    win.RunDynamicWizard(onlineCams, selectedCams, getPositions, setStegmanns)
    return win

#----------------------------------------------------------------------

def CreateWizard( parent ) :
    ''' Creates the Wizard for the GUI.
        Expects the parent to be a window for the wizard parent and also to
        offer an API that lets the wizard request services.
        <- get OnlineCams
        <- get SelectedCams
        <-trigger get positions
        <-set stegman
        ->asynch callback with positions dict
        '''
    wizard = StegWizard( parent,
                    parent.GetOnlineCams,
                    parent.GetSelectedCams,
                    parent.GetHeadPositions,
                    parent.SetStegmanns )
    print 'created wizard'
    return wizard


if __name__ == '__main__':

    from wx.lib.mixins.inspection import InspectionTool

    def getPositions( cam, callback=None ):
        ''' returns position dict for given [cam]
        '''
        print 'getPositions called'
        wx.FutureCall( 2000, sendPositions, callback )
        return None
        
    def sendPositions(*args):
        print 'send', args
        args[0]({ 'pan' : 10200, 'tilt':6789,'height':3456,'zoom' : 3000, 'focus': 678,'x':4567,'y':378954 } )


    def setStegmanns( cam, settings ) :
        ''' sets cam to given position
        '''
        print 'set camera', cam, 'to', settings

#2 With this test system I can get a crash when the wizard exits but
#2 it does not seem to happen within the GUI.

#2 Not fully retested in this mode after changing prints to log statements

    import sys 
    from wx.lib.mixins.inspection import InspectableApp 
    app = InspectableApp(False) 
    #app=wx.App()
    frame = wx.Frame( parent=None )
    log=logging.getLogger('')
    log.addHandler(logging.StreamHandler())
    runTest(frame, frame, lambda:set([4,7,9,12,3]), lambda:set([ 7]),getPositions, setStegmanns)
    frame.Show(True) 
    app.MainLoop() 


